//
//  Int+HTTPStatusCode.swift
//  P Nashr
//
//  Created by apple on 5/24/21.
//

import Foundation

extension Int {
  public var isSuccessHTTPCode: Bool {
    return 200 <= self && self < 300
  }
}
