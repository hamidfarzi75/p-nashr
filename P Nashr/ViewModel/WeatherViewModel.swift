//
//  WeatherViewModel.swift
//  P Nashr
//
//  Created by apple on 5/25/21.
//

import UIKit

public final class WeatherViewModel{
    
    let response : Response
    var country: String?
    var city: String?
    var temperature: String?
    var minTemperature: String?
    var maxTemperature: String?
    var feelsLike: String?
    
    init(response: Response) {
        self.response = response
        
        if let sys = response.sys, let country = sys.country{
            self.country = country
        }else{
            self.country = "Unknown"
        }
        
        if let name = response.name {
            self.city = name
        }
        
        guard let main = response.main else {
            self.temperature = "not specified!"
            self.minTemperature = "not specified!"
            self.maxTemperature = "not specified!"
            self.feelsLike = "not specified!"
            return
        }
        
        if let temperature = main.temp{
            let temperatureInCelsius = Int(temperature - 273)
            self.temperature = "\(temperatureInCelsius)°C"
        }else{
            self.temperature = "not specified!"
        }
        
        if let minTemperature = main.tempMin{
            let temperatureInCelsius = Int(minTemperature - 273)
            self.minTemperature = "\(temperatureInCelsius)°C"
        }else{
            self.minTemperature = "not specified!"
        }
        
        if let maxTemperature = main.tempMax{
            let temperatureInCelsius = Int(maxTemperature - 273)
            self.maxTemperature = "\(temperatureInCelsius)°C"
        }else{
            self.maxTemperature = "not specified!"
        }
        
        if let feelsLike = main.feelsLike{
            let temperatureInCelsius = Int(feelsLike - 273)
            self.feelsLike = "\(temperatureInCelsius)°C"
        }else{
            self.feelsLike = "not specified!"
        }
    }
}
