//
//  ServerRequest.swift
//  P Nashr
//
//  Created by apple on 5/24/21.
//

import Foundation

class ServerRequest{
    
    let baseURL = "https://api.openweathermap.org/"
    let appId = "3a98c957cad7ef3936b2c9de4800e270"
    let session = URLSession.shared
    
    public static let shared = ServerRequest()
    
    func getWeather(lat: String, lon : String, success _success: @escaping (Response)-> Void,
                         failure _failure: @escaping (NetworkError)-> Void){
        let success: (Response) -> Void = { response in
            DispatchQueue.main.async { _success(response)}
        }
        
        let failure: (NetworkError)-> Void = { error in
            DispatchQueue.main.async { _failure(error) }
        }
        guard let url = URL(string: baseURL + ("data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(appId)")) else {
            fatalError()
        }
        
        let task = session.dataTask(with: url) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode.isSuccessHTTPCode,
                  let data = data else{
                if let error = error{
                    failure(NetworkError(error: error))
                }
                else{
                    failure(NetworkError(response: response))
                }
                return
            }
            
            do{
                let response = try JSONDecoder().decode(Response.self, from: data)
                success(response)
            }
            catch{
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
}
