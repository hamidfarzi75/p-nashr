//
//  WeatherViewController.swift
//  P Nashr
//
//  Created by apple on 5/24/21.
//

import UIKit
import MapKit

class WeatherViewController: UIViewController {

    //MARK: - Injections
    var weatherViewModel: WeatherViewModel!
    
    //MARK: - Instance Properties
    var selectedCoordinate: CLLocationCoordinate2D?
    
    
    //MARK: - Outlets
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var minTemperature: UILabel!
    @IBOutlet weak var maxTemperature: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        getWeather()
    }
    
    // MARK: - Functions
    func getWeather(){
        guard let lat = selectedCoordinate?.latitude, let long = selectedCoordinate?.longitude else {
            fatalError()
        }
        ServerRequest.shared.getWeather(lat: "\(lat)", lon: "\(long)") { [weak self] response in
            if let strongSelf = self
            {
                strongSelf.weatherViewModel = WeatherViewModel(response: response)
                strongSelf.country.text = strongSelf.weatherViewModel.country
                strongSelf.city.text = strongSelf.weatherViewModel.city
                strongSelf.temperature.text = strongSelf.weatherViewModel.temperature
                strongSelf.minTemperature.text = strongSelf.weatherViewModel.minTemperature
                strongSelf.maxTemperature.text = strongSelf.weatherViewModel.maxTemperature
                strongSelf.feelsLike.text = strongSelf.weatherViewModel.feelsLike
            }
        } failure: { [weak self] error in
            if let _ = self
            {
                print(error)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
