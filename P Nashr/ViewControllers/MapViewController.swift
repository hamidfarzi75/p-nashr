//
//  MapViewController.swift
//  P Nashr
//
//  Created by apple on 5/24/21.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    //MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showWeather(sender:)))
        mapView.addGestureRecognizer(tapGesture)
    }
    
    // MARK: - Functions
    @objc func showWeather(sender: UITapGestureRecognizer)
    {
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        performSegue(withIdentifier: "goToWeather", sender: locationCoordinate)
        print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let weatherViewController = segue.destination as? WeatherViewController{
            weatherViewController.selectedCoordinate = (sender as! CLLocationCoordinate2D)
        }
    }
}
